package com.pspaceplus.driver.bacnetdemo;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class BacnetdemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(BacnetdemoApplication.class, args);
    }

}
